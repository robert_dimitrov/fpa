package aufgabenplaner;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Ui_Schritt extends Ui_Aufgabe {

	protected final JTextField erledigt = new JTextField();

	private Ui_Schritt() {
		super();
		fieldPanel.add(new JLabel("Erledigt-Zeitpunkt:"));
		fieldPanel.add(erledigt);
		buttonPanel.add(new JButton("Erledigen"));
	}

	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new Ui_Schritt();
			}
		});
	}

}
