package aufgabenplaner;

import javax.swing.JLabel;
import javax.swing.JTextField;

public class Ui_Vorhaben extends Ui_Aufgabe {

	protected final JTextField endtermin = new JTextField();

	private Ui_Vorhaben() {
		super();
		restStunden.setText("10");
		restStunden.setEditable(false);
		istStunden.setText("0");
		istStunden.setEditable(false);
		fieldPanel.add(new JLabel("End-Termin:"));
		fieldPanel.add(endtermin);
		
	}

	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new Ui_Vorhaben();
			}
		});
	}

}
