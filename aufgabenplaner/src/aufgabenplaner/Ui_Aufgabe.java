package aufgabenplaner;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public abstract class Ui_Aufgabe extends JDialog {

	protected final int fieldWidth = 30;
	protected final String[] dummyVorhaben = { "FPA-Aufgaben lösen",
			"Neue Sprache erlernen", "Mehr Sport" };
	protected final JTextField id = new JTextField("123");
	protected final JTextField titel = new JTextField("", fieldWidth);
	protected final JTextField restStunden = new JTextField("", fieldWidth);
	protected final JTextField istStunden = new JTextField("", fieldWidth);
	protected final JTextField status = new JTextField("", fieldWidth);
	protected final JTextArea beschreibung = new JTextArea(5, fieldWidth);
	protected final JComboBox teil = new JComboBox(dummyVorhaben);
	protected final JPanel buttonPanel = new JPanel();
	protected final JPanel fieldPanel = new JPanel();

	protected enum AufgabeStatus {
		NEU, IN_BEARBEITUNG, ERLEDIGT
	}

	public Ui_Aufgabe() {

		Container container = getContentPane();
		container.setLayout(new BorderLayout());

		fieldPanel.setLayout(new GridLayout(0, 1));
		
		fieldPanel.add(new JLabel("ID:"));
		fieldPanel.add(id);
		fieldPanel.add(new JLabel("Titel:"));
		fieldPanel.add(titel);
		fieldPanel.add(new JLabel("Beschreibung:"));
		fieldPanel.add(beschreibung);
		fieldPanel.add(new JLabel("Teil von Vorhaben:"));
		fieldPanel.add(teil);
		fieldPanel.add(new JLabel("Rest-Stunden zu arbeiten:"));
		fieldPanel.add(restStunden);
		fieldPanel.add(new JLabel("Ist-Stunden gearbeitet:"));
		fieldPanel.add(istStunden);
		fieldPanel.add(new JLabel("Status:"));
		fieldPanel.add(status);

		status.setText(AufgabeStatus.NEU.toString());
		status.setEditable(false);
		id.setEditable(false);

		buttonPanel.setLayout(new FlowLayout());
		buttonPanel.add(new JButton("Erfassen"));
		buttonPanel.add(new JButton("Ändern"));
		buttonPanel.setSize(300, 100);

		container.add(fieldPanel, BorderLayout.CENTER);
		container.add(buttonPanel, BorderLayout.SOUTH);

		addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(final java.awt.event.WindowEvent i_) {
				System.exit(0);
			}
		});

		setSize(300, 500);
		setResizable(false);
		setVisible(true);

	}

}